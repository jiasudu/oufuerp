# 红毛六曜星ERP

#### Description
红毛六曜星ERP基于OFBiz，Ant Design Vue，提供开源免费、用户体验好的ERP系统，解决管理难、数据统计难的问题，实现业务透明化、简易化。专注于企业内部管理，主要包括基础信息、商品中心、采购管理、销售管理、仓储管理、物流管理等。同时支持对Tenant，集团，公司，部门、岗位、人员等进行细化角色权限管理。非常适合中大型企业或需要高度定制化的公司。为企业带来一体化的解决方案。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
