# 红毛六曜星ERP

#### 介绍
红毛六曜星ERP基于OFBiz，Ant Design Vue，提供开源免费、用户体验好的ERP系统，解决管理难、数据统计难的问题，实现业务透明化、简易化。专注于企业内部管理，主要包括基础信息、商品中心、采购管理、销售管理、仓储管理、物流管理等。同时支持对Tenant，集团，公司，部门、岗位、人员等进行细化角色权限管理。非常适合中大型企业或需要高度定制化的公司。为企业带来一体化的解决方案。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
